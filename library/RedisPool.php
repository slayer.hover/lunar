<?php

use Swoole\Coroutine\Channel as channel;
class RedisPool
{
    protected static $pool; //连接池容器

	public static function init()
    {
        if(!self::$pool) {
			$cnf        = getConfig();
            self::$pool = new channel($cnf->swoole->task_num);
			for ($i = 0; $i < $cnf->swoole->task_num; $i++)
			{
				$res = new \Cache();
				self::$pool->push($res);
			}
        }
        return self::$pool;
    }

    public static function put($redis)
    {
        self::init()->push($redis);
    }

    public static function get()
    {
        return self::init()->pop();
    }
}
