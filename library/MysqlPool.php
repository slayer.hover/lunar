<?php

use Swoole\Coroutine\Channel as channel;
class MysqlPool
{
    private static $pool;      //连接池容器

    public static function init()
    {
        if(!self::$pool) {
            $conf        = getConfig();
            $dbset    =    array(
                'dsn'        =>    'mysql:host='. $conf->mysql->host . ';dbname=' . $conf->mysql->database,
                'name'        =>    $conf->mysql->user,
                'password'    =>    $conf->mysql->password,
            );
            self::$pool = new channel($conf->swoole->task_num);
            for ($i = 0; $i < $conf->swoole->task_num; $i++)
            {
                $res = new \DB($dbset);
                self::$pool->push($res);
            }
        }
        return self::$pool;
    }

    public static function put($mysql)
    {
        self::init()->push($mysql);
    }

    public static function get()
    {
        return self::init()->pop();
    }

}
