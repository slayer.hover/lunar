<?php
#mysql&&redis参数
function getConfig()
{
    $config = new stdClass();
    $config->mysql  = (object)[
        'host'     => '127.0.0.1',
        'port'     => 3306,
        'user'     => 'root',
        'password' => 'root',
        'database' => 'lunar',
    ];
    $config->redis  = (object)[
        'enable'   => true,
        'hosts'    => "127.0.0.1",
        'ports'    => 6379,
        'expire'   => 86400,
        'auth'     => "",
        'selectDB' => 5,
        'prefix'   => "",
    ];

    return $config;
}
