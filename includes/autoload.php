<?php
spl_autoload_register(function ($class) {
    if ($class) {
        $file = str_replace('\\', '/', $class);
        $file = ROOT_DIR . '/' . $file . '.php';
        if (file_exists($file)) {
            require($file);
        } else {
            $file = ROOT_DIR . '/library/' . $class . '.php';
            if (file_exists($file)) {
                require($file);
            } else {
                file_put_contents(ROOT_DIR . "/logs/autoload.log", $file . PHP_EOL, FILE_APPEND);
            }
        }
    }
});
